package repositories;

import models.User;

import java.util.List;

public interface CrudRepository<ID, T> {

    List<T> findAll();

    void save(T entity);
    void update(T entity);
    void delete(T entity);
    void deleteById(ID entity);
    T findByID(ID id);


}
