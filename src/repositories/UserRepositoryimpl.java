package repositories;

import models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryimpl implements UserRepository{

    private String fileName;
    private List<User> users = new ArrayList<>();

    public UserRepositoryimpl(String fileName){
        this.fileName = fileName;
        this.users = findAll();
        System.out.print(users);

    }
    @Override
    public List findAll() {


        try{
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String s;
            while((s = reader.readLine()) != null){

                users.add(new User(s));
            }
            reader.close();
            return users;
        }
        catch(IOException e){
            throw new IllegalArgumentException(e);
        }
    }



    @Override
    public void save(User entity) {

        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = entity.toString();
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(User entity) {

        User userToDelete = findByID(entity.getId());

        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter("users.txt"));

            for(int i = 0; i < users.size(); i++){

                if(userToDelete.getId().equals(users.get(i).getId())){
                    continue;
                }

                writer.write(users.get(i).toString());
                writer.newLine();
            }

            writer.close();
        }
        catch(IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(UUID entity) {

        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter("users.txt"));

            for(int i = 0; i < users.size(); i++){

                if(entity.equals(users.get(i).getId())){
                    continue;
                }

                writer.write(users.get(i).toString());
                writer.newLine();
            }

            writer.close();
        }
        catch(IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findByID(UUID o) {

        for(int i = 0; i < users.size(); i++){
            if(users.get(i).getId().equals(o)){
                return users.get(i);
            }
        }
        return null;
    }
}
